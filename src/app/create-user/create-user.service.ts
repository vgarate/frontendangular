import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { RestResponse } from '../model/restResponse.model';
import { UserModel } from '../model/user.model';

@Injectable({
  providedIn: 'root'
})
export class CreateUserService {

  constructor(private http: HttpClient) { }

  /**
   * Validación de campos obligatorios
   * @param user
   */
  public validate(user: UserModel): boolean{
    let isValid = true;

    if (!user.primerNombre || !user.primerApellido || !user.email) {
      isValid = false;
    }
    
    return isValid;
  }
  
  public saveOrUpdate(user: UserModel): Observable<RestResponse> {
    return this.http.post<RestResponse>("http://localhost:7878/saveOrUpdate", JSON.stringify(user));
  }

}
