import { ParentEntity } from "./parentEntity.model";


export class UserModel extends ParentEntity{

    public primerNombre: string;
    public segundoNombre: string;
    public primerApellido: string;
    public segundoApellido: string;
    public email: string;
    public direccion: string;
}