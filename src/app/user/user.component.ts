import { Component, OnInit } from '@angular/core';

import { UserService } from './user.service';
import { UserModel } from '../model/user.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
  providers: [UserService]
})
export class UserComponent implements OnInit {
  private users: Array<UserModel>;

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit() {
    this.loadUsers();
  }

/**
 * Cargar los usuarios a la lista
 */
  private loadUsers(): void{
    this.userService.getUsers().subscribe( res => {
      this.users = res;
      console.log(res);
    });
  }

  /**
   * Editar el usuario
   */
  public edit(user: UserModel): void {
    sessionStorage.setItem('user', JSON.stringify(user));
    this.router.navigate(['/createUserComponent']);
  }

  /**
   * Borrar usuario
   */
  public delete(user: UserModel): void {
    this.userService.delete(user);
  }
}
