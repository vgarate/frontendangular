# FrontEndAngular

Proyecto generado con [Angular CLI](https://github.com/angular/angular-cli) version 7.0.6, pero actualizado a 7.2 y `Bootstrap`

## Development server

Corre `ng serve` para iniciar el servidor. Dirigite a `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Instalación

Después de clonar éste proyecto debes ejecutar `npm install` para agregar los módulos y despues bajar el BackEnd realizado con SpringBoot desde [aquí](https://gitlab.com/Radamhantiz/spring-boot-y-angular.git)

## Conexión con el Backend

para realizar la conexión con el backend, descarga un plugin de chrome llamado [Allow-Control](https://chrome.google.com/webstore/detail/allow-control-allow-origi/nlfbmbojpeacfghkpbjhddihlkkiljbi/related) y en Intercepter URLs or URL patterns agrega `http://localhost:7878/` que es donde se encuentra el BackEnd. Recuerda cambiar el puerto en el plugin si lo cambias en el BackEnd de Spring.